package com.mycompany.aplicationone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplicationOneApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplicationOneApplication.class, args);
	}

}
