package com.mycompany.aplicationone.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class GreetingsController {
   @GetMapping("/welcome")
    public String Welcome(){
        return "Bienvenido al mundo de dockers y GitLab desde su pagina web";
    }
}
